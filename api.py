from flask import Flask, flash, redirect, render_template, request, session, abort
import os, mysql.connector

app = Flask(__name__)

# Connect to the database

data_base = mysql.connector.connect(
        host="localhost",
        user="myusername",
        passwd="mypassword",
        database="mydatabase")

data_base_cursor = data_base.cursor()

# app's routes

@app.route('/')
def api_home():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        return "Login successful !"

@app.route('/login', methods=['POST'])
def api_login():
    login = request.form['username']
    password = request.form['password']
    session['logged_in'] = verify_login_password(login, password)
    if not session['logged_in']:
        flash('Wrong password!')
    return api_home()

def verify_login_password(login, password):
    sql_query = "SELECT * FROM users WHERE login = '" +login+ "' AND password = '" +password+ "';" 
    data_base_cursor.execute(sql_query)
    query_result = data_base_cursor.fetchall()
    return len(query_result) != 0   # True if the couple login/password exists in the database.

if __name__ == "__main__":
    app.run()

##############
# CORRECTION #
##############

def verify_login_password_correction_1(login, password):
    sql_query = ("SELECT * FROM users WHERE login = %s AND password = %s;" )
    data_base_cursor.execute(sql_query,(login, password))
    query_result = data_base_cursor.fetchall()
    return len(query_result) != 0   # True if the couple login/password exists in the database.


FORBIDDEN_CHR=["'", ";", "-", "=", "*"]

def verify_login_password_correction_2(login, password):
    for c in FORBIDDEN_CHR:
        if c in login or c in password :
            return False
    sql_query = "SELECT * FROM users WHERE login = '" +login+ "' AND password = '" +password+ "';" 
    data_base_cursor.execute(sql_query)
    query_result = data_base_cursor.fetchall()
    return len(query_result) != 0   # True if the couple login/password exists in the database. 


def verify_login_password_correction_3(login, password):
    for c in FORBIDDEN_CHR:
        if c in login or c in password :
            return False
    sql_query = ("SELECT * FROM users WHERE login = %s AND password = %s;" )
    data_base_cursor.execute(sql_query,(login, password))
    query_result = data_base_cursor.fetchall()
    return len(query_result) != 0   # True if the couple login/password exists in the database.