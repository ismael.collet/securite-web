# Exercice 1
Voici une liste des vulnérabilités que nous avons trouvé dans ce code (dans l'ordre d'apparition) :
* La clé de chiffrement est inscrite en dur dans le code source. Cela peut être problématique si ce dernier n’est pas stocké dans un endroit sûr, ou si un employé partage le code source en pensant qu’il n’y a pas d’information sensible à l’intérieur.
* Le « SECURE_DIRECTORY » ne l’est pas.
* **Il n’y a pas d’authentification** pour accéder à la liste des employés. N’importe qui (par exemple un ancien employé, un attaquant ayant deviné l'irl ou unutilisant un logiciel de bruteforce) peut accéder à la page contenant des données personnelles sur les employés, y compris leur numéro de sécurité sociale.
