# Exercice 2
Nous avons choisi le sujet 1 :
```
Vous pouvez proposer une API Web dont le service interagi avec une base de donnée. Vous implanterez ce service en le laissant sensible aux injections SQL. 
Contre mesure possible, sanitisation des entrées ou utilisation d'un framework de mapping objet relationnel 
```
## Notre vulnérabilité
Notre API se trouve dans le fichier *api.py*.
La vulnérabilité se cache dans ces lignes :
```
def verify_login_password(login, password):
        sql_query = "SELECT * FROM users WHERE login = '" +login+ "' AND password = '" +password+ "';"  
    data_base_cursor.execute(sql_query)
    query_result = data_base_cursor.fetchall()
    return len(query_result) != 0   # True if the couple login/password exists in the database.
```

En effet, un attaquant peut effectuer une injection SQL, par exemple en saisissant comme mot de passe `’OR 1=1--`. En effet la requête devient
```
SELECT * FROM users WHERE login = ‘[login]’ AND password = ‘’ OR 1=1;"
```
`1=1` Étant toujours vrai, la requête renvoie toute la table et l'attaquant passe la vérification sans avoir à connaître le mot de passe de l’utilisateur ciblé.

 ## La correction de la vulnérabilité
Deux solution ont été trouvées : utiliser la sécurité apportée par la librairie mySQL et interdire la saisie de certains symboles.

### Utilisation des fonctionnalités de la librairie mySQL
```
def verify_login_password_correction_1(login, password):
    sql_query = ("SELECT * FROM users WHERE login = %s AND password = %s;" )
    data_base_cursor.execute(sql_query,(login, password))
    query_result = data_base_cursor.fetchall()
    return len(query_result) != 0   # True if the couple login/password exists in the database.
```
En utilisant mySQL de cette façon, les chaînes de caractères saisies vont être insérées dans la requête comme des chaînes de caractères et non comme une partie du script. Bien sûr, on suppose que la librairie a été bien codée.

###  Interdire la saisie de certains symboles
Si on doit utiliser une librairie moins sécurisée que MySQL (par exemple une librairie fait maison), il peut être judicieux d’interdire l’utilisation de certains symboles dans la requête.
```
FORBIDDEN_CHR=["'", ";", "-", "=", "*"]

def verify_login_password_correction_2(login, password):
    for c in FORBIDDEN_CHR:
        if c in login or c in password :
            return False
    sql_query = "SELECT * FROM users WHERE login = '" +login+ "' AND password = '" +password+ "';" 
    data_base_cursor.execute(sql_query)
    query_result = data_base_cursor.fetchall()
    return len(query_result) != 0   # True if the couple login/password exists in the database. 
```

### Utilisation des deux méthodes
On peut utiliser les deux méthodes. Ainsi, si plus tard les programmeurs veulent changer de librairie, il y aura toujours une couche de protection.
```
def verify_login_password_correction_3(login, password):
    for c in FORBIDDEN_CHR:
        if c in login or c in password :
            return False
    sql_query = ("SELECT * FROM users WHERE login = %s AND password = %s;" )
    data_base_cursor.execute(sql_query,(login, password))
    query_result = data_base_cursor.fetchall()
    return len(query_result) != 0   # True if the couple login/password exists in the database.
```